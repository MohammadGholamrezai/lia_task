<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Response\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AssigneController extends Controller
{
    public function permissionToRole(Request $request)
    {
        $this->authorize('assign_permission_role');
        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 1
        ]);

        return ApiResponse::Json(200, ApiResponse::$storedSuccessfully, [], 200);
    }

    public function roleToUser(Request $request)
    {
        $this->authorize('assign_role_user');
        DB::table('role_user')->insert([
            'role_id' => 1,
            'user_id' => 7
        ]);

        return ApiResponse::Json(200, ApiResponse::$storedSuccessfully, [], 200);
    }
}
