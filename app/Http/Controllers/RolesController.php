<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Response\ApiResponse;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    private $roles;

    public function __construct(RoleRepository $roles)
    {
        $this->roles = $roles;
    }

    public function index()
    {
        $this->authorize('view_roles');
        $roles = $this->roles->all();
        return ApiResponse::Json(200, '', $roles, 200);
    }

    public function store(Request $request)
    {
        $this->authorize('register_role');
        $role = $this->roles->create([
            'name' => 'user',
            'label' => 'user'
        ]);

        return ApiResponse::Json(200, ApiResponse::$storedSuccessfully, $role, 200);
    }

    public function update($id, Request $request)
    {
        $this->authorize('update_role');
        $role = $this->roles->find($id);
        $update = [
            'name' => '',
            'label' => ''
        ];
        $this->roles->update($role, $update);

        return ApiResponse::Json(200, ApiResponse::$updateSuccessfully, [], 200);
    }

    public function destroy($id)
    {
        $this->authorize('delete_role');
        $role = $this->roles->find($id);
        $this->roles->delete($role);

        return ApiResponse::Json(200, ApiResponse::$deleteSuccessfully, [], 200);
    }
}
