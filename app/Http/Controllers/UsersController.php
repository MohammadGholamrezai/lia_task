<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Response\ApiResponse;
use App\Repositories\UsersRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    private $users;

    public function __construct(UsersRepository $users)
    {
        $this->users = $users;
    }

    public function index()
    {
        $this->authorize('view_users');
        $users = $this->users->all();
        return ApiResponse::Json(200, '', $users->makeHidden(['password']), 200);
    }

    public function store(Request $request)
    {
        $user = $this->users->create([
            'name' => 'mohammad',
            'email' => 'm.gholamrezai@gmail.com',
            'password' => 'pass',
            'status' => 0
        ]);

        return ApiResponse::Json(200, ApiResponse::$storedSuccessfully, $user->makeHidden(['password']), 200);
    }

    public function activeUser($id)
    {
        $this->authorize('active_users');
        $user = $this->users->find($id);
        $update = [
            'status' => 1
        ];
        $this->users->update($user, $update);

        return ApiResponse::Json(200, ApiResponse::$updateSuccessfully, [], 200);
    }

    public function update($id, Request $request)
    {
        $this->authorize('update_users');

        if (in_array('user', Auth::user()->roles()->pluck('name')) && $id !== Auth::id())
            return ApiResponse::Json(400, ApiResponse::$noHaveAccess, [], 400);

        $user = $this->users->find($id);
        $update = [
            'name' => 'mohammadReza',
            'email' => 'sample_email@sample.com'
        ];
        $this->users->update($user, $update);

        return ApiResponse::Json(200, ApiResponse::$updateSuccessfully, [], 200);
    }

    public function destroy($id)
    {
        if (in_array('user', Auth::user()->roles()->pluck('name')) && $id !== Auth::id())
            return ApiResponse::Json(400, ApiResponse::$noHaveAccess, [], 400);

        $user = $this->users->find($id);
        $this->users->delete($user);

        return ApiResponse::Json(200, ApiResponse::$deleteSuccessfully, [], 200);
    }
}
