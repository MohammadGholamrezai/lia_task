<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Response\ApiResponse;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    private $permissions;

    public function __construct(PermissionRepository $permission)
    {
        $this->permissions = $permission;
    }

    public function store(Request $request)
    {
        $this->authorize('register_permission');
        $permission = $this->permissions->create([
            'name' => 'update_users',
            'label' => 'update_users'
        ]);

        return ApiResponse::Json(200, ApiResponse::$storedSuccessfully, $permission, 200);
    }

    public function update($id, Request $request)
    {
        $this->authorize('update_permission');
        $permission = $this->permissions->find($id);
        $update = [
            'name' => '',
            'label' => ''
        ];
        $this->permissions->update($permission, $update);

        return ApiResponse::Json(200, ApiResponse::$updateSuccessfully, [], 200);
    }

    public function destroy($id)
    {
        $this->authorize('delete_permission');
        $permission = $this->permissions->find($id);
        $this->permissions->delete($permission);

        return ApiResponse::Json(200, ApiResponse::$deleteSuccessfully, [], 200);
    }
}
