<?php


namespace App\Http\Controllers\Response;


class errors
{
    private $errors_fa = [
        'Delete successfully' => 'با موفقیت حذف شد',
        'Not found Data' => 'داده ای پیدا نشد',
        'Unauthorized' => 'احراز هویت نشده',
        'Bad Request' => 'درخواست اشتباه',
        'Stored successfully' => 'با موفقیت ثبت شد',
        'Store failed' => '',
        'Updated successfully' => 'به روز رسانی با موفقیت انجام شد',
        'User not activated by admin!' => 'حساب کاربر توسط ادمین فعال نشده است!'
    ];

    public function errorGet($lang, $errorMsg = null)
    {
        if ($lang == 'fa') {
            if (isset($this->errors_fa[$errorMsg])) {
                return $this->errors_fa[$errorMsg];
            } else {
                return $errorMsg;
            }
        } else {
            return $errorMsg;
        }
    }


}
