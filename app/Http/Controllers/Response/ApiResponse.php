<?php


namespace App\Http\Controllers\Response;


class ApiResponse
{
    static $deleteSuccessfully = "Delete successfully";
    static $notFoundData = "Not found Data";
    static $Unauthorized = "Unauthorized";
    static $BadRequest = "Bad Request";
    static $storedSuccessfully = "Stored successfully";
    static $storeFail = "Store failed";
    static $TokenHasExpired = "Token has expired";
    static $noHaveAccess = "You dont have role to access";
    static $updateSuccessfully = "Updated successfully";
    static $notActivated = "User not activated by admin!";

    static function Json($status, $msg, $data = [], $statusSystem = 200)
    {
        $e = new errors();

        return response()->json([
            'status' => $status,
            'message' => $e->errorGet('fa', $msg),
            'data' => $data

        ], $statusSystem);
    }
}
