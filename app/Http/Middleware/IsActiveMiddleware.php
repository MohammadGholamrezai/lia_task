<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Response\ApiResponse;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array('user', Auth::user()->roles()->pluck('name')) && Auth::user()->status === 0)
            return ApiResponse::json(400, ApiResponse::$notActivated, [], 400);

        return $next($request);
    }
}
