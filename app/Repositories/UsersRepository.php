<?php


namespace App\Repositories;


use App\Models\User;

class UsersRepository extends Repository
{
    public function model()
    {
        return User::class;
    }
}
