<?php

use Illuminate\Http\Request;
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return app()->version();
});

$router->get('test', 'UsersController@index');

$router->group(['prefix' => 'api/v1/oauth'], function() use (&$router) {
    $router->post('token', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');
});

$router->group(['prefix' => 'api/v1', 'namespace' => '\App\Http\Controllers'], function (&$router) {
    $router->post('users', 'UsersController@store');
    $router->get('users/login', 'LoginController@login');
});

$router->group(['prefix' => 'api/v1', 'namespace' => '\App\Http\Controllers', 'middleware' => ['auth:api', 'isActive']], function (&$router) {
    $router->get('users/index', 'UsersController@index');
    $router->put('users/{id}', 'UsersController@update');
    $router->delete('users/{id}', 'UsersController@destroy');

    $router->post('roles', 'RolesController@store');
    $router->post('permissions', 'PermissionsController@store');
});
